Chart Testing

ct is the the tool for testing Helm charts. It is meant to be used for linting and testing pull requests. It automatically detects charts changed against the target branch.

ct is a command-line application. All command-line flags can also be set via environment variables or config file. Environment variables must be prefixed with CT_. Underscores must be used instead of hyphens.

CLI flags, environment variables, and a config file can be mixed. The following order of precedence applies:

    CLI flags
    Environment variables
    Config file

Note that linting requires config file for yamllint and yamale. If not specified, these files are search in the current directory, $HOME/.ct, and /etc/ct, in that order. Samples are provided in the etc folder.

See more info at: https://github.com/helm/chart-testing