ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/opensource/python/python38
ARG BASE_TAG=3.8

FROM quay.io/helmpack/chart-testing:v3.4.0 AS chart-testing

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

USER root

RUN dnf update -y && \
    dnf install -y --nodocs git && \
    dnf clean all && \
    mkdir /packages/

COPY *.whl /packages/

COPY --from=chart-testing --chown=root:root /usr/local/bin/ct /usr/local/bin/
COPY --from=chart-testing --chown=root:root /usr/local/bin/helm /usr/local/bin/
COPY --from=chart-testing --chown=root:root /usr/local/bin/kubectl /usr/local/bin/
COPY --from=chart-testing --chown=root:root /etc/ct/* /etc/ct/

RUN pip3 install -U --no-index \
        /packages/pip-21.2.3-py3-none-any.whl \
        /packages/PyYAML-5.4.1-cp38-cp38-manylinux1_x86_64.whl \
        /packages/yamale-3.0.8-py3-none-any.whl && \
    chmod +x \
        /usr/local/bin/ct \
        /usr/local/bin/helm \
        /usr/local/bin/kubectl

USER 1000

# Ensure that the binary is available on path and is executable
RUN ct --help
